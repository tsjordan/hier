use std::env;
use std::io;
use itertools::Itertools;
use colored::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    let delim= if args.len() > 1 { &args[1] } else {""};
    highlight_lines(delim);
}

/// Highlights common beginnings of consecutive lines piped from stdin. 
/// Takes a delimiter string as an argument
pub fn highlight_lines(delim: &str) {
    let mut last_line = String::new();
    loop {
        let mut line = String::new();
        io::stdin().read_line(&mut line).expect("Failed to read stdin");

        let common = line.split(delim).zip(last_line.split(delim))
                             .take_while(|(a, b)| a == b).map(|(a, _)| a)
                             .join(delim);

        let unique = line.trim_start_matches(&common);
        print!("{}{}", common.black().on_bright_black(), unique);

        last_line = line;

        if last_line.is_empty() {
            break;
        }
    }
}

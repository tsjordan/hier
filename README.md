# Hier
The program highlights common beggining of consecutive lines piped from STDIN obeying a delimiter which is passed as a parameter. 

## Usage
`find ~/ | hier '/'` highlights common directory structures. Passing no delimiter does a character-by-character highlight.

`ll | hier ' '` shows files in your directory with common permissions, owners, names, etc...